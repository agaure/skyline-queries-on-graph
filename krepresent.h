#include <iostream>
#include <bits/stdc++.h>
#define INF 999999999

using namespace std;

class cmpk{
    public:
        bool operator() (pair<int, int> a, pair<int, int> b){
            return a.first > b.first;
        }
};
struct DjksK{
        set< pair<int, int>> setds;
        vector<int> dist;//(V, INF);
        vector<int> par;//(V, INF);
};
class GraphK
{
    public:
    int V, Q = 0;    // No. of vertices
    long long int nocmp = 0;
    float x = 0.3, y = 0.7, k = 3;
    vector<int>query;
    list<pair<int, int>> *adj;
 

    GraphK(int V);  // Constructor
 
    // function to add an edge to graph
    void addEdge(int u, int v, int w);
 
    // prints shortest path from s
    void skylines();
    
    //declare query vertex
    void setquery(int id);

    //check if all shortest path ended.
    bool allEmpty(vector<DjksK*>& djstack);

    bool isDomin(int u, int v, vector<vector<int>>& shtdist);
};
 
// Allocates memory for adjacency list
GraphK::GraphK(int V)
{
    this->V = V;
    adj = new list<pair<int, int>>[V];
}
 
void GraphK::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

void GraphK::setquery(int id){
	query.push_back(id);
    Q++;	
}

// Prints shortest paths from src to all other vertices
void GraphK::skylines()
{   
	vector<vector<int>>shtdist(V, vector<int>(Q, INF));
    cout << "Running Djkstra.." << endl;
	vector<set<pair<int, int>>> Aggr(Q);
    vector<struct DjksK*>djstack;
    for (int q = 0; q < Q; ++q)
    {
        struct DjksK* djks = new DjksK;
        int src = query[q];
        djks->dist = vector<int>(V, INF);
        djks->par = vector<int>(V, INF);
        (djks->setds).insert(make_pair(0, src));
        (djks->dist)[src] = 0;
        (djks->par)[src] = src;
        djstack.push_back(djks);
    }
    int fhit;
    bool flag = true;
    vector<int>visit(V);
    clock_t start1 = clock();
    while(!allEmpty(djstack))
    {
    	for(int q = 0; q < Q; ++q){
    		if((djstack[q]->setds.empty())){
    			continue;
			}
    	    pair<int, int> tmp = *(((djstack[q])->setds).begin());
        	((djstack[q])->setds).erase(((djstack[q])->setds).begin());
            int u = tmp.second;
            
            shtdist[u][q] = tmp.first;
            if(flag){
                visit[u]++;
	            (Aggr[q]).insert(tmp);
            }
            if(visit[u] == Q && flag){
                flag = /**/false;
                fhit = u;
                if(!(djstack[q]->setds.empty())){
	            	pair<int, int> tmp1 = *(((djstack[q])->setds).begin());
            		if(tmp1.first == tmp.first){
            			(Aggr[q]).insert(tmp1);
            		}
            	}
                //break;
            }
            list<pair<int, int>>::iterator i;
            for (i = adj[u].begin(); i != adj[u].end(); ++i)
            {
                int v = (*i).first;
                int weight = (*i).second;
                if (djstack[q]->dist[v] > djstack[q]->dist[u] + weight)
                {
                    if (djstack[q]->dist[v] != INF)
                        ((djstack[q])->setds).erase(((djstack[q])->setds).find(make_pair(djstack[q]->dist[v], v)));
                    djstack[q]->dist[v] = djstack[q]->dist[u] + weight;
                    djstack[q]->par[v] = u;
                    ((djstack[q])->setds).insert(make_pair(djstack[q]->dist[v], v));
                }
            }
            if(!(djstack[q]->setds.empty())){
	            pair<int, int> tmp1 = *(((djstack[q])->setds).begin());
            	if(tmp1.first == tmp.first){
            		q--;
            	}
            }
    	}
    }

    for(int i : query){
        for(int j = 0; j < Q; j++){
            shtdist[i][j] = INF;
        }
    }
    cout << "Complete.\n";
    clock_t end1 = clock();
    double time1 = (double) (end1-start1) / CLOCKS_PER_SEC * 1000.0;
    cout << "Running Time: " << time1 <<" ms\n"<< endl;
 	unordered_set<int>sky;
    cout << "Finding Skylines..\n";
    int s = 0;
    for(int q = 0; q < Q; q++){
        s += Aggr[q].size();
    }
 	clock_t start2 = clock();
 	for(int q = 0; q < Q; q++){
 		while(!((Aggr[q]).empty())){
 			set<pair<int, int>>::iterator it = next(Aggr[q].begin());
 			pair<int, int> tmp1 = (*((Aggr[q]).begin()));
 			(Aggr[q]).erase((Aggr[q]).begin());
 			bool isSky = true;
    		for(; it != ((Aggr[q]).end()); ){
 				pair<int, int>tmp2 = (*it);
 				isSky = isSky && (!isDomin(tmp1.second, tmp2.second, shtdist));
				if(isDomin(tmp2.second, tmp1.second, shtdist)){

 					Aggr[q].erase(it++);
 					//it--;
 				} else{
 					++it;
 				}
 				if(!isSky){
 					break;
 				}
 			}
            for(int q = 0; q < Q; q++){
                if(query[q] == tmp1.second){
                    isSky = false;
                }
            }
 			if(isSky){
 				if(sky.find(tmp1.second) == sky.end()){
 					sky.insert(tmp1.second);
 				}
 			}
 		}
 	}
    clock_t end2 = clock();
    double time2 = (double) (end2-start2) / CLOCKS_PER_SEC * 1000.0;
    cout << "Complete.\n";
    cout << "Running Time: " << time2 << "ms\n" << endl;

    priority_queue<pair<int, int>, vector<pair<int,int>>, cmpk>pqs;
    unordered_set<int>::iterator it = sky.begin();
    for(; it != sky.end(); it++){
        float sum = 0, mini = INF;
       // cout << (*it) << ". ";
        for(int q = 0; q < Q; q++){
            sum += shtdist[(*it)][q];
            mini = min(shtdist[(*it)][q], (int)mini);
           // cout << shtdist[(*it)][q] << " " ;
        }
        float score = sum*x + mini*y;

        //cout <<" "<< score << "\n";
        pqs.push(make_pair(score, (*it)));
    }
    cout << "First seen in all query tree: " << fhit << endl;
    cout << "No. of Comparisions: " << nocmp << endl;
    cout << "Skyline set size: " << sky.size() << endl;
    cout << "k-Representatives: ";
    it = sky.begin();
    for(int i = 0; i < k; i++){
        cout << pqs.top().second << " " ;
        pqs.pop();
    }
    cout << endl << endl;
}

bool GraphK::allEmpty(vector<DjksK*>& djstack){
    std::vector<DjksK*>::iterator it;
    bool em = true;
    for(it = djstack.begin(); it != djstack.end(); it++)
    {
      em = em && ((*it)->setds).empty();  
    }
    return em; 
}

bool GraphK::isDomin(int u, int v,  vector<vector<int>>& shtdist){
	bool c1 = true;
	bool c2 = false;
	for(int q= 0; q < Q; q++){
		c1 = c1 && (shtdist[v][q] <= shtdist[u][q]);
		c2 = c2 || (shtdist[v][q] < shtdist[u][q]);
	}
    nocmp++;
	return (c1 && c2);
}

extern GraphK graphK;