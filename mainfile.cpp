#include <iostream>
#include <bits/stdc++.h>
#include "aggregation.h"
#include "naive.h"
#include "onepass.h"
#include "krepresent.h"
#define INF 999999999

using namespace std;

int main()
{
    // create the graph given in above fugure
    int V = 4039;
    int u, v;
    float w = 1;
    ifstream infile("Data/1.txt");
    infile >> V;
    GraphA ga(V);
    GraphN gn(V);
    GraphO go(V);
    GraphK gk(V);
    int E = 0;
    while(infile >> u >> v){
        ga.addEdge(u, v, w);
        gn.addEdge(u, v, w);
        go.addEdge(u, v, w);
        gk.addEdge(u, v, w);
        E++;
    }
    infile.close();
    ifstream infileq("Data/query1.txt");
    int qi;
    while(infileq >> qi){
     	ga.setquery(qi);
        gn.setquery(qi);
        go.setquery(qi);
        gk.setquery(qi);
    }      
    cout << "Nodes in Graph: " << V << endl;
    cout << "Edges in Graph: " << E << endl;
    cout << "Query Vertex: " << ga.Q <<"\nEnter Naive:1 Aggregation:2 One-Pass:3 K-Representative:4" << endl << endl;
    int ex = 1;
    while(ex){
        clock_t start = clock();
        int a;
        cout << "Enter: ";
        cin >> a;
        switch(a){
            case 1: gn.skylines();
                    break;
            case 2: ga.skylines();
                    break;
            case 3: go.skylines();
                    break;
            case 4: gk.skylines();
                    break;
            default: ex = 0;
                    break;
        }
 
        clock_t end = clock();
        double time = (double) (end-start) / CLOCKS_PER_SEC * 1000.0;
        cout << "Total Running Time: " << time << "ms" << endl <<"-----------------------------------------------------------------------------"<<endl;        
    }
    return 0;
}
 