//PROVISION FOR MULTIPLE ITERATIONS AND ASK USER FOR K
#include<stdio.h>
#include<stdlib.h>

struct vertex
{
	int ident;	
	int * flag; //0 no path found; 1 a path found; 2 added to tree;
	long long int * dist;
	int count;
	long long int index_value;
	double topk;
	int sky_flag;
};

struct vertex temp;
long long int expo(long long int a, int b)
{
	long long int ans = 1;
	
	while(b>0)
	{
		ans = ans * a;
		b--;	
	}
	return ans;
	
}

struct cluster
{
	int cluster_count;
	int inherited_count;
	int lsky_count;
	int * index_array; //STORED DATA ID OF THE VERTICES IN THE CLUSTER
	int * lsky_array; //WHICH HAVE BECOME SKYLINES 
	int * isky_array; //ID OF THE VERTICES WHICH ARE GLOBALLY SKYLINES
}; 

struct vertex * v_array;
struct cluster * cluster_array;

int * query;
int query_count;
int * new_add;
int launcher;
long long int min;
int min_index;
long long int ** weight_matrix;
long long int weight;
int vertex1;
int vertex2;
float w;
int launch;
int i, j, k;
int p, q;
int no_edges;
int no_vertex;
int pqv;
int was_something_added;
int already_found;
long long int cluster_count;
long long int recieved_index;
int sky_count;
int kid_index;
int k_top;
FILE * ptr;
FILE * ptr2;
int total_tree_count = 0;
int total_comp_count = 0;

int check_for_dominators(int clstr, int vertex, int mode) //TAKES CLUSTER NO AND VERTEX INDEX need to be careful
{
	int a, b, c, count1, count2;
	int flag = 1;
	int actual_vertex = cluster_array[clstr].index_array[vertex];
	int comp_vertex;
	
	total_comp_count++;

	if(mode == 0)
	{
		
		
		for(a=0;a<cluster_array[clstr].cluster_count;a++)
		{
			// only check with other vertices a and vertex are both indices
			{
				b = cluster_array[clstr].index_array[a];
				
				count1 = 0; //wins of b
				count2 = 0;//wins of actual vertex

				for(c=1;c<=query_count;c++) //dimension by dimension
				{
					if(v_array[actual_vertex].flag[c] == 1 && v_array[b].flag[c] == 1) //check common dimensions
					{
						if(v_array[actual_vertex].dist[c] > v_array[b].dist[c])	
							count1++;
						else if(v_array[actual_vertex].dist[c] < v_array[b].dist[c])
							count2++;				
					} 				
				}

				if(count1 > 0 && count2 == 0) //counting of wins for each player
				{
					flag = 0;
					break;				
				}
				
							
			}		
		}
	}
	else if(mode == 1) //else if is important
	{
		
		
		for(a=0;a<cluster_array[clstr].inherited_count;a++)
		{
						
			 // only check with other vertices a and vertex are both indices
			{
				b = cluster_array[clstr].isky_array[a];
				
				count1 = 0; //wins of b
				count2 = 0;//wins of actual vertex

				for(c=1;c<=query_count;c++) //dimension by dimension
				{
					if(v_array[actual_vertex].flag[c] == 1 && v_array[b].flag[c] == 1) //check common dimensions
					{
						if(v_array[actual_vertex].dist[c] > v_array[b].dist[c])	
							count1++;
						else if(v_array[actual_vertex].dist[c] < v_array[b].dist[c])
							count2++;				
					} 				
				}

				if(count1 > 0 && count2 == 0) //counting of wins for each player
				{
					flag = 0;
					break;				
				}
				
							
			}		
		}
	}
	return flag;
} 


void relax(int launch_vertex, int target_vertex, int query)
{
	long long int base_dist;
	long long int jump;
	
	//printf("\nWe have entered Relax function:");

	base_dist = v_array[launch_vertex].dist[query];
	jump = weight_matrix[launch_vertex][target_vertex];

	//printf("\nDistance from pqv %d to %d is %lld", query, launch_vertex, base_dist);
	//printf("\nDistance from %d to %d is %lld", launch_vertex, target_vertex, jump);

	
	if(v_array[target_vertex].flag[query] == 0)
	{
		v_array[target_vertex].dist[query] = base_dist+jump;
		v_array[target_vertex].flag[query] = 1;
		
	}
	else if(v_array[target_vertex].flag[query] == 1)
		if(v_array[target_vertex].dist[query] > base_dist+jump)
		{
			v_array[target_vertex].dist[query] = base_dist+jump;
			v_array[target_vertex].flag[query] = 1;	
			
		}
}

struct info
{
	int length;
	long long int * arr;
};

struct info kid_info(long long int x)
{
	fprintf(ptr2, "\nFinding the descendants of %lld", x);
		
	long long int verifier;
	int count;
	int loop;
	struct info ans;
	verifier = 1;
	count = 0;
	verifier = 2;
	for(loop = 1;loop<=query_count;loop++) //COUNTING ONES IN BINARY REPRESENTATION
	{
		printf("\nVerifier = %lld", verifier); 		
		if((verifier & x) == verifier && x != verifier)
			count++;
		verifier = verifier*2;
	}
	ans.length = count;
	printf("\nFound %d", count);
	ans.arr = (long long int *)calloc(count, sizeof(long long int));
	
	verifier = 2;
	for(loop = 1;loop<=query_count;loop++)
	{
		if((verifier & x) == verifier && x!=verifier)
		{
			ans.arr[--count] = x-verifier;
		}		
		verifier = verifier*2;
	}
		
	return ans;
}

struct info info1;

void send_info(int receiver_cluster_index, int vertex_index)
{
	fprintf(ptr2, "\nSkyline %d is being sent to cluster %d", vertex_index, receiver_cluster_index);	
	int x = cluster_array[receiver_cluster_index].inherited_count; 
	int a, b, c;
	b = 0;
	for(a=0;a<x;a++)
		if(cluster_array[receiver_cluster_index].isky_array[a] == vertex_index)
		{
			fprintf(ptr2, "\nFound %d", cluster_array[receiver_cluster_index].isky_array[a]);			
			b = 1;
			break;		
		}
	
	if(b==0)
	{
		fprintf(ptr2, "\nAdding %d to cluster %d", vertex_index, receiver_cluster_index);
		fprintf(ptr2, "\nx = %d", x);		
		cluster_array[receiver_cluster_index].isky_array[x] = vertex_index;
		fprintf(ptr2, "\nCLUSTER = %d", receiver_cluster_index);
		fprintf(ptr2, "\nveryex_added = %d", vertex_index);
		fprintf(ptr2, "\nVerifier = %d", cluster_array[receiver_cluster_index].isky_array[x]);
		cluster_array[receiver_cluster_index].inherited_count++;
		fprintf(ptr2, "\n%d has inherited count incremented to %d", receiver_cluster_index,
									cluster_array[receiver_cluster_index].inherited_count);  	
	}
}


int main()
{
	ptr = fopen("cousr.txt", "r");
	ptr2 = fopen("Mega_Answer.txt", "w");
		
	
	fscanf(ptr, "%d",&no_vertex);
	
	fscanf(ptr, "%d", &no_edges);

	v_array = (struct vertex *)calloc(no_vertex+1, sizeof(struct vertex));
	
	
	fscanf(ptr, "%d", &query_count);
	query = (int *)calloc(query_count+1, sizeof(int));
	new_add = (int *)calloc(query_count+1, sizeof(int));

	
	weight_matrix = (long long int **)calloc(no_vertex+1, sizeof(long long int *));
	for(i=1;i<=no_vertex;i++)
		weight_matrix[i] = (long long int *)calloc(no_vertex+1, sizeof(long long int));

	for(i=1;i<=no_vertex;i++)
		for(j=1;j<=no_vertex;j++)
			weight_matrix[i][j] = -1;

			
	for(i=1;i<=no_vertex;i++)
	{
		v_array[i].flag = (int *)calloc(query_count+1, sizeof(int));
		v_array[i].dist = (long long int *)calloc(query_count+1, sizeof(long long int));
	}		

	printf("\nScanning edges");
	for(i=0;i<no_edges;i++)
	{
		fscanf(ptr, "%d %d %f", &vertex1, &vertex2, &w);
		printf("%d %d\n", vertex1, vertex2);
		weight_matrix[vertex1+1][vertex2+1] = w;
		weight_matrix[vertex2+1][vertex1+1] = w; 
		printf("Next\n");		
	}	
	printf("\nStill working?? Please Press 1 and Enter:");	
	scanf("%d", &p);

	for(i=1;i<=query_count;i++)
	{
		
		fscanf(ptr, "%d", &query[i]);	
		printf("\n%d", query[i]);
		query[i]++;
	}
	printf("\nReached");
	sky_count = 0;
	//ALLOCATING ARRAY FOR NEW VERTEX
	
	for(i=1;i<=query_count;i++)
	{
		new_add[i] = query[i];	
	}
	
	printf("\nReached2");
	for(i=1;i<=no_vertex;i++)
	{
		fprintf(ptr2, "\n");		
		for(j=1;j<=no_vertex;j++)
			fprintf(ptr2, "w %lld\t", weight_matrix[i][j]);
	}		
	int outer_while = 1;

	for(i=1;i<=query_count;i++) //PUTTING EACH VERTEX IN ITS OWN TREE
	{
		printf("\nQuery vertex %d", query[i]);		
		v_array[query[i]].flag[i] = 2;
		v_array[query[i]].count++;
		v_array[query[i]].index_value += expo(2, i);	//IMPORTANT
	}
	
	printf("\nReached3");
	printf("\nEnter how many vertices you want in the main intersect:");
	scanf("%d", &outer_while);
	while(outer_while)
	{ //
		for(i=1;i<=query_count;i++)
		{// //
			printf("\nFOR QUERY VERTEX i = %d", i);		
			pqv = query[i];
			printf("\nExpanding Tree %d, index is %d", pqv, i);		
			launch = new_add[i];
			v_array[launch].flag[i] = 2;
		
			printf("\nAdding %d to Tree of %d", launch, pqv);
		
			for(j=1;j<=no_vertex;j++)  //Neighbours from launch
			{// // //			
						
				if(launch!=j)			
				if(weight_matrix[launch][j] !=-1) //edge exists from launcher to potential target vertex
				{// // // // 
					if(v_array[j].flag[i] != 2) // target is not already in the tree
					{// // // // //
						printf("\nRelaxing %d %d for Tree %d", launch, j, pqv); 					
						relax(launch, j, i);		
					}// // // // //			
				}// // // //			
			}// // //
			printf("\nCalculating Next Entrant in Tree %d", pqv);
			printf("\nHoodibaba\n");		
			//finding next entrant		
			min = 999999999;
			was_something_added = 0;		
			for(j=1;j<=no_vertex;j++)
			{// // //
						
				printf("\nj - %d", j);				
				if(j!=pqv && v_array[j].flag[i] == 1 && j!=launch)
				{// // // //
					printf("\nThere exists path from %d to %d", pqv, j);
					printf("\nDistance from %d to %d is %lld", pqv, j, v_array[j].dist[i]);				
					if(v_array[j].dist[i] < min)
					{// // // // //
						min = v_array[j].dist[i];
						min_index = j;	
						printf("\nNew min is %d", j);
						was_something_added = 1;			
					}
				}// // // //		
			}// // //
			if(was_something_added == 1)
			{// // //		
				new_add[i] = min_index;
				v_array[min_index].flag[i] = 2; 
				v_array[min_index].count++;
				v_array[min_index].index_value += expo(2, i);
				v_array[min_index].topk += 1/((double)(min));
				printf("\nVertex %d dist value %lld; Priority Value %lf", min_index, min, v_array[min_index].topk);
				printf("\nVertex %d has IndexValue %lld", min_index, v_array[min_index].index_value);
				printf("\nVertex %d is added to tree of %d", min_index, pqv);
				printf("\nVertex %d is a part of to %d trees", min_index, v_array[min_index].count);
				total_tree_count++;
			}

			// // //
			printf("\n_____________________________________");
			printf("\nTree of %d\n", pqv);
			for(k=1;k<=no_vertex;k++)
			{// // //
								
				if(v_array[k].flag[i] == 2)
					printf("%d\t", k);			
			}// // //
			
			printf("\nHoohhaa");
			printf("\npqv has %d kids", v_array[min_index].count);
			if(v_array[min_index].count == query_count)
			{// // //
				printf("Vertex %d is complete", min_index);			
				outer_while--;				
				break;		
			}// // //

		}// //
		printf("\nOuter while = %d", outer_while);
	}//
		
		
		printf("\nCLINTOn");

		cluster_count = 0;
	
		for(k=1;k<=query_count;k++)
		{
			cluster_count += expo(2, k);
		}
		printf("\nCLUSTER COUNT %lld\n", cluster_count);
		cluster_array = (struct cluster *)calloc(cluster_count+1, sizeof(struct cluster));
	
		for(i=1;i<=cluster_count;i++)
		{
			cluster_array[i].index_array = (int *)calloc(no_vertex+1,sizeof(int));	
			cluster_array[i].lsky_array = (int *)calloc(no_vertex+1,sizeof(int));	
			cluster_array[i].isky_array = (int *)calloc(no_vertex+1,sizeof(int));	
		}
	
		for(i=1;i<=no_vertex;i++)
		{
			printf("\ni = %d", i);			
			k = v_array[i].index_value;
			printf("\ni belongs to cluster %d", k);
			if(k == 0)
			{
				printf("\nNot added to any tree yet");
							
			}
			else
			{
				j = cluster_array[k].cluster_count;
				printf("\nThere are %d other vertices already in %d", j, k);
				cluster_array[k].index_array[j] = i;
				cluster_array[k].cluster_count++;
			}		
		}
		
		printf("\nExites");
		for(i=cluster_count;i>=1;i--) //GOING THROUGH CLUSTERS IN REVERSE ORDER
		{
			printf("\nDealing with Cluster %d", i);		
			if(i%2 == 0)
			{
				fprintf(ptr2, "\n_________________________NEW CLUSTER ________________-");		
				printf("\nWe know that %d has %d vertices", i, cluster_array[i].cluster_count);			
				for(j=0;j<cluster_array[i].cluster_count;j++)
				{
				//cluster_array[i].index_array[j] is the id of the vertex
					fprintf(ptr2, "\nChecking if %d is a skyline in cluster %d",cluster_array[i].index_array[j], 															i);			
			
					k = check_for_dominators(i, j, 0); //MEMBERS OF INDEX ARRAY i is the cluster no j is the index 
					
					if(k == 1)
					{
						printf("\nsemi sky criteria for j = %d", j);				
						k = check_for_dominators(i, j, 1); //INHERITED COMP
						
						printf("\nk - %d", k);
						if(k == 1) //BECOMES A SURE SHOT LOCAL SKYLINE IN THAT BOX
						{
											
						cluster_array[i].lsky_array[cluster_array[i].lsky_count] = cluster_array[i].index_array[j];
						
		fprintf(ptr2, "\n*****************SKYLINE %d placed at index %d", cluster_array[i].lsky_array[cluster_array[i].lsky_count], 										cluster_array[i].lsky_count);					
					
							cluster_array[i].lsky_count++;
							v_array[cluster_array[i].index_array[j]].sky_flag = 1;	
						sky_count++;
						}
					}
				
				}
	
				printf("\nPass Down Phase");
				fprintf(ptr2, "\n_______PASS DOWN PHASE");
				//PASS DOWN SKYLINES
				info1 = kid_info(i);
				
				printf("\nLen received - %d", info1.length);
			
				for(j=0;j<info1.length;j++)
				{
					kid_index = info1.arr[j];
					printf("\nKiddo");
					if(kid_index!=0)
					{				
						
				
						for(p=0;p<cluster_array[i].lsky_count;p++)
						{
							q = cluster_array[i].lsky_array[p];
							
							send_info(kid_index, q); //sends local skyline q to kid_index			
						}
				
						for(p=0;p<cluster_array[i].inherited_count;p++)
						{
											
							q = cluster_array[i].isky_array[p]; //sends inherited_skylibne q to kid index
							send_info(kid_index, q);			
						}	
					}
					else
					{
						printf("\nReached bottom tip");
					}
				}
			}		
		}	
	fclose(ptr);
	fclose(ptr2);
	printf("\nFOund %d skylines", sky_count);
	printf("\nEnter K:");
	
	scanf("%d", &k_top);

	for(i=1;i<=no_vertex;i++)
		v_array[i].ident = i;


	for(i=1;i<=no_vertex;i++)
	{
		for(j = i+1;j<=no_vertex;j++)
		{
			if(v_array[i].sky_flag == 0 && v_array[j].sky_flag == 1)
			{
				temp = v_array[i];
				v_array[i] = v_array[j];
				v_array[j] = temp;			
			}
			else if(v_array[i].sky_flag == 1 && v_array[i].sky_flag == 1 && v_array[i].topk < v_array[j].topk)
			{
				temp = v_array[i];
				v_array[i] = v_array[j];
				v_array[j] = temp;					
			}			
		}		
	}
	printf("\nSize of The trees: %d", total_tree_count);
	printf("\nTotal number of dominance checks: %d", total_comp_count);
	printf("\nMost prominent skylines :");

	for(i=1;i<=k_top;i++)
	{
		printf("\n%d", v_array[i].ident);
		printf("\nAssociated topk value = %lf", v_array[i].topk);				
	}
	
	printf("\n");
	
	
	return 0;
}

