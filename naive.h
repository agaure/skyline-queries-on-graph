#include <iostream>
#include <bits/stdc++.h>
#define INF 999999999

using namespace std;

class GraphN
{
public:
    int V, Q = 0; 
    long long int nocmp = 0;   // No. of vertices
    vector<int>query;
    list<pair<int, int>> *adj;
 
public:
    GraphN(int V);  // Constructor
 
    // function to add an edge to GraphN
    void addEdge(int u, int v, int w);
 
    // prints shortest path from s
    void skylines();
    
    //declare query vertex
    void setquery(int id);

  

    bool isDomin(int u, int v, vector<vector<int>>& shtdist);
};
 
// Allocates memory for adjacency list
GraphN::GraphN(int V)
{
    this->V = V;
    adj = new list<pair<int, int>>[V];
}
 
void GraphN::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

void GraphN::setquery(int id){
	query.push_back(id);
    Q++;	
}

// Prints shortest paths from src to all other vertices
void GraphN::skylines()
{   
    cout << "Finding shortest Paths..\n";
    clock_t startdj = clock();
	vector<vector<int>>shtdist(V, vector<int>(Q, INF));
    for(int q = 0; q < Q; ++q)
    {
        vector<int>dist(V, INF);
        set< pair<int, int>> setds;
        int src = query[q];
        setds.insert(make_pair(0, src));
        dist[src] = 0;
    	while(!setds.empty()){
    	    pair<int, int> tmp = *(setds.begin());
        	setds.erase((setds).begin());
            int u = tmp.second;
            shtdist[u][q] = tmp.first;
            list<pair<int, int>>::iterator i;
            for (i = adj[u].begin(); i != adj[u].end(); ++i)
            {
                int v = (*i).first;
                int weight = (*i).second;
                if (dist[v] > dist[u] + weight)
                {
                    if (dist[v] != INF)
                        setds.erase(setds.find(make_pair(dist[v], v)));
                    dist[v] = dist[u] + weight;
                    setds.insert(make_pair(dist[v], v));
                }
            }
    	}
    }

    clock_t enddj = clock();
    double timedj = (double) (enddj-startdj) / CLOCKS_PER_SEC * 1000.0;
    cout << "complete.\nRunning Time: " << timedj << "ms" << endl << endl;
 	
    clock_t start2 = clock();
    list<int>skywin;
            //BNL
   /* for(int i = 0; i < V; i++){
        bool issky = true;
        list<int>::iterator it;
        for(it = skywin.begin(); it != skywin.end(); it++){
            if(isDomin(i, (*it), shtdist)){
                issky = false;
            }else{
                if(isDomin((*it), i, shtdist)){                                
                    skywin.erase(it);
                    it--;
                }
            }
        }
        if(issky){
            skywin.push_back(i);
        }
    }*/
    for(int i : query){
        for(int j = 0; j < Q; j++){
            shtdist[i][j] = INF;
        }
    }
            //Naive   
    cout << "Finding Skylines..\n";
    for(int i = 0; i < V; i++){
        bool issky = true;
        for(int j = 0; j < V; j++){
            issky = issky && (!isDomin(i, j, shtdist));
            if(!issky) break;
        }
        for(int q = 0; q < Q; q++){
            if(query[q] == i){
                issky = false;
            }
            if(shtdist[i][q] == INF){
            	issky = false;
            }
        }
        if(issky){
            skywin.push_back(i);
        }
    }

    clock_t end2 = clock();
    double time2 = (double) (end2-start2) / CLOCKS_PER_SEC * 1000.0;
    cout << "done.\nRuning Time: " << time2 << " ms" << endl;
    cout << "No. of Comparisions: " << nocmp << endl;
    cout << "Skyline Set Size: " << skywin.size() << endl << endl;
    cout << "Skylines: \n";
    list<int>::iterator it = skywin.begin();
    for(; it != skywin.end(); it++){
    	cout << (*it) << " ";// << shtdist[(*it)][0] << " " << shtdist[(*it)][1] << " " << shtdist[(*it)][2] << "\n";
    }
    cout << endl << endl;
}

bool GraphN::isDomin(int u, int v,  vector<vector<int>>& shtdist){
	bool c1 = true;
	bool c2 = false;
	for(int q= 0; q < Q; q++){
		c1 = c1 && (shtdist[v][q] <= shtdist[u][q]);
		c2 = c2 || (shtdist[v][q] < shtdist[u][q]);
	}
    nocmp++;
	return (c1 && c2);
}

extern GraphN graphN;