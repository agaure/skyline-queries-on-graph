#include <iostream>
#include <bits/stdc++.h>
#define INF 999999999

using namespace std;

class cmp{
    public:
        bool operator() (pair<int, int> a, pair<int, int> b){
            return a.first > b.first;
        }
};

struct DjksO{
        set< pair<int, int>> setds;
        vector<int> dist;//(V, INF);
        vector<int> par;//(V, INF);
};

class GraphO
{
    public:
    int V, Q = 0;    // No. of vertices
    long long int nocmp = 0;
    float x = 0.4, y = 0.6, k = 10;
    vector<int>query;
    list<pair<int, int>> *adj;
 

    GraphO(int V);  // Constructor
 
    // function to add an edge to GraphO
    void addEdge(int u, int v, int w);
 
    // prints shortest path from s
    void skylines();
    
    //declare query vertex
    void setquery(int id);

    //check if all shortest path ended.
    bool allEmpty(vector<DjksO*>& djstack);

    bool isDomin(int u, int v, vector<vector<int>>& shtdist);
};
 
// Allocates memory for adjacency list
GraphO::GraphO(int V)
{
    this->V = V;
    adj = new list<pair<int, int>>[V];
}
 
void GraphO::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

void GraphO::setquery(int id){
    query.push_back(id);
    Q++;    
}

// Prints shortest paths from src to all other vertices
void GraphO::skylines()
{   
    vector<vector<int>>shtdist(V, vector<int>(Q, INF));
    cout << "Running Q-Djkstra and finding skylines in one pass.." << endl;
    vector<struct DjksO*>djstack;
    for (int q = 0; q < Q; ++q)
    {
        struct DjksO* djks = new DjksO;
        int src = query[q];
        djks->dist = vector<int>(V, INF);
        djks->par = vector<int>(V, INF);
        (djks->setds).insert(make_pair(0, src));
        (djks->dist)[src] = 0;
        (djks->par)[src] = src;
        djstack.push_back(djks);
    }
    int fhit;
    bool flag = true;
    vector<int>visit(V);
    clock_t start1 = clock();
    set<pair<int, int>>sky;
    while(!allEmpty(djstack))
    {
        for(int q = 0; q < Q; ++q){
            if((djstack[q]->setds.empty())){
                continue;
            }
            pair<int, int> tmp = *(((djstack[q])->setds).begin());
            ((djstack[q])->setds).erase(((djstack[q])->setds).begin());
            int u = tmp.second;
            visit[u]++;            
            shtdist[u][q] = tmp.first;
            int entrop = 0;
            for(int i = 0; i < Q; i++){
                entrop += shtdist[u][q];
            }
            bool isq = false;
            for(int y : query){
                if(u == y){
                    isq = true;
                    for(int i = 0; i < Q; i++){
                        shtdist[u][i] = INF;
                    }
                }
                    
            }
            if(visit[u] == Q && !isq){
                set<pair<int, int>>::iterator itr = sky.begin();
                bool issky = true;
                for(; itr != sky.end();){
                    issky = issky && (!isDomin(u, (*itr).second, shtdist));
                    if(isDomin((*itr).second, u, shtdist)){
                        sky.erase(itr++);
                    } else{
                        ++itr;
                    }
                    if(isDomin(u, (*itr).second, shtdist)){
                        issky = false;
                        break;
                    }
                }
                for(int q = 0; q < Q; q++){
                    if(query[q] == u){
                        issky = false;
                    }
                }
                if(issky){
                    sky.insert(make_pair(entrop, u));
                }
            }
            list<pair<int, int>>::iterator i;
            for (i = adj[u].begin(); i != adj[u].end(); ++i)
            {
                int v = (*i).first;
                int weight = (*i).second;
                if (djstack[q]->dist[v] > djstack[q]->dist[u] + weight)
                {
                    if (djstack[q]->dist[v] != INF)
                        ((djstack[q])->setds).erase(((djstack[q])->setds).find(make_pair(djstack[q]->dist[v], v)));
                    djstack[q]->dist[v] = djstack[q]->dist[u] + weight;
                    djstack[q]->par[v] = u;
                    ((djstack[q])->setds).insert(make_pair(djstack[q]->dist[v], v));
                }
            }
            if(!(djstack[q]->setds.empty())){
                pair<int, int> tmp1 = *(((djstack[q])->setds).begin());
                if(tmp1.first == tmp.first){
                    q--;
                }
            }
        }
    }

    cout << "Complete.\n";
    clock_t end1 = clock();
    double time1 = (double) (end1-start1) / CLOCKS_PER_SEC * 1000.0;
    cout << "Running Time: " << time1 <<" ms\n"<< endl;
    cout << "No. of Comparisions: " << nocmp << endl;
    cout << "Skyline set size: " << sky.size() << endl;
    set<pair<int, int>>::iterator itr = sky.begin();
    for(; itr != sky.end(); itr++){
        cout << (*itr).second << " ";
    }
    cout << endl;
}

bool GraphO::allEmpty(vector<DjksO*>& djstack){
    std::vector<DjksO*>::iterator it;
    bool em = true;
    for(it = djstack.begin(); it != djstack.end(); it++)
    {
      em = em && ((*it)->setds).empty();  
    }
    return em; 
}

bool GraphO::isDomin(int u, int v,  vector<vector<int>>& shtdist){
    bool c1 = true;
    bool c2 = false;
    for(int q= 0; q < Q; q++){
        c1 = c1 && (shtdist[v][q] <= shtdist[u][q]);
        c2 = c2 || (shtdist[v][q] < shtdist[u][q]);
    }
    nocmp++;
    return (c1 && c2);
}

extern GraphO graphO;